package pl.fudala.mateusz.quiz.frontend;

import lombok.Data;

@Data
public class GameOptions {
    private int numberOfQuestions;
    private QuestionLevel questionLevel;
    private int categoryId;
}