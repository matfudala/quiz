package pl.fudala.mateusz.quiz.frontend;

import lombok.Data;

@Data
public class UserAnswer {
    private String answer;
}
