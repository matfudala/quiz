package pl.fudala.mateusz.quiz.frontend;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.fudala.mateusz.quiz.services.OngoingGameService;
import pl.fudala.mateusz.quiz.services.QuizDataService;

@Controller
@Log
@RequestMapping
public class FrontController {

    private static final String REDIRECT = "redirect:";

    private final QuizDataService qds;
    private final OngoingGameService ogs;

    @Autowired
    public FrontController(QuizDataService qds, OngoingGameService ogs) {
        this.qds = qds;
        this.ogs = ogs;
    }

    @GetMapping
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/select")
    public String select(Model model) {
        model.addAttribute("gameOptions", new GameOptions());
        model.addAttribute("categories", qds.getQuizCategories());
        return "select";
    }

    @PostMapping("/select")
    public String postSelectForm(Model model, @ModelAttribute GameOptions gameOptions) {
        log.info("Form submitted with data: " + gameOptions);
        ogs.init(gameOptions);
        return REDIRECT + "game";
    }

    @GetMapping("/game")
    public String game(Model model) {
        model.addAttribute("userAnswer", new UserAnswer());
        model.addAttribute("currentQuestionNumber", ogs.getCurrentQuestionNumber());
        model.addAttribute("totalQuestionNumber", ogs.getTotalQuestionNumber());
        model.addAttribute("currentQuestion", ogs.getCurrentQuestion());
        model.addAttribute("currentQuestionAnswers", ogs.getCurrentQuestionAnswersInRandomOrder());
        return "game";
    }

    @PostMapping("/game")
    public String postSelectForm(Model model, @ModelAttribute UserAnswer userAnswer) {
        ogs.checkAnswerForCurrentQuestionAndUpdatePoints(userAnswer.getAnswer());
        boolean hasNextQuestion = ogs.proceedToNextQuestion();
        return hasNextQuestion ? REDIRECT + "game" : REDIRECT + "summary";
    }

    @GetMapping("/summary")
    public String summary(Model model) {
        model.addAttribute("questionLevel", ogs.getQuestionLevel());
        model.addAttribute("categoryName", ogs.getCategoryName());
        model.addAttribute("points", ogs.getPoints());
        model.addAttribute("maxPoints", ogs.getTotalQuestionNumber());
        return "summary";
    }
}
