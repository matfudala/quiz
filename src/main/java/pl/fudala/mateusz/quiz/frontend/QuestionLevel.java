package pl.fudala.mateusz.quiz.frontend;

import java.util.Collection;
import java.util.EnumSet;

public enum QuestionLevel {
    EASY,
    MEDIUM,
    HARD;

    public QuestionLevel getClosestLevel() {
        switch (this) {
            case EASY:
                return MEDIUM;
            case MEDIUM:
                return HARD;
            case HARD:
                return MEDIUM;
        }
        return null;
    }

    public static QuestionLevel calculateNextLevel(Collection<QuestionLevel> levels){
        if (levels == null || levels.isEmpty()){
            return null;
        }
        if (levels.size() == 1) {
            return levels.iterator().next().getClosestLevel();
        }
        EnumSet<QuestionLevel> missingLevels = EnumSet.complementOf(EnumSet.copyOf(levels));
        if (missingLevels.isEmpty()){
            return null;
        } else {
            return missingLevels.iterator().next();
        }
    }
}

