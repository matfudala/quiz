package pl.fudala.mateusz.quiz.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.fudala.mateusz.quiz.frontend.QuestionLevel;

@NoArgsConstructor
@ToString
public class CategoryQuestionCountInfoDto {

    @JsonProperty("category_id")
    private int categoryId;

    @JsonProperty("category_question_count")
    private CategoryQuestionCountDto categoryQuestionCount;

    @NoArgsConstructor
    @ToString
    @Getter
    public static class CategoryQuestionCountDto {
        @JsonProperty("total_questions_count")
        private int totalQuestionCount;
        @JsonProperty("total_easy_question_count")
        private int totalEasyQuestionCount;
        @JsonProperty("total_medium_question_count")
        private int totalMediumQuestionCount;
        @JsonProperty("total_hard_question_count")
        private int totalHardQuestionCount;
    }

    public int getTotalQuestionCount() {
        return categoryQuestionCount.getTotalQuestionCount();
    }

    public int getQuestionCountForQuestionLevel(QuestionLevel questionLevel) {
        switch (questionLevel) {
            case EASY:
                return categoryQuestionCount.getTotalEasyQuestionCount();
            case MEDIUM:
                return categoryQuestionCount.getTotalMediumQuestionCount();
            case HARD:
                return categoryQuestionCount.getTotalHardQuestionCount();
            default:
                return 0;
        }
    }
}
