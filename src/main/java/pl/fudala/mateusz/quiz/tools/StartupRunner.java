package pl.fudala.mateusz.quiz.tools;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.fudala.mateusz.quiz.entities.PlayerEntity;
import pl.fudala.mateusz.quiz.repositories.PlayerRepository;
import pl.fudala.mateusz.quiz.services.QuizDataService;

@Component
@Log
public class StartupRunner implements CommandLineRunner {

    private final QuizDataService qds;

    @Autowired
    public StartupRunner(QuizDataService qds) {
        this.qds = qds;
    }

    @Override
    public void run(String... args) throws Exception {
    log.info("Startup actions:");
    qds.getQuizCategories();
    }
}
