package pl.fudala.mateusz.quiz.services;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.fudala.mateusz.quiz.dto.CategoriesDto;
import pl.fudala.mateusz.quiz.dto.QuestionsDto;
import pl.fudala.mateusz.quiz.frontend.GameOptions;
import pl.fudala.mateusz.quiz.frontend.QuestionLevel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@SessionScope
@Log
public class OngoingGameService {
    private GameOptions gameOptions;
    private int currentQuestionIndex;
    private int points;

    private List<QuestionsDto.QuestionDto> questions;

    private final QuizDataService qds;

    @Autowired
    public OngoingGameService(QuizDataService qds) {
        this.qds = qds;
    }

    public void init(GameOptions gameOptions) {
        this.gameOptions = gameOptions;
        this.currentQuestionIndex = 0;
        this.points = 0;

        this.questions = qds.getQuizQuestions(gameOptions);
    }

    public int getCurrentQuestionNumber() {
        return currentQuestionIndex + 1;
    }

    public int getTotalQuestionNumber() {
        return questions.size();
    }

    public String getCurrentQuestion() {
        QuestionsDto.QuestionDto dto = questions.get(currentQuestionIndex);
        return dto.getQuestion();
    }

    public List<String> getCurrentQuestionAnswersInRandomOrder() {
        QuestionsDto.QuestionDto dto = questions.get(currentQuestionIndex);

        List<String> answers = new ArrayList<>();
        answers.add(dto.getCorrectAnswer());
        answers.addAll(dto.getIncorrectAnswers());

        Collections.shuffle(answers);
        return answers;
    }

    public void checkAnswerForCurrentQuestionAndUpdatePoints(String userAnswer) {
        QuestionsDto.QuestionDto dto = questions.get(currentQuestionIndex);
        boolean correct = dto.getCorrectAnswer().equals(userAnswer);
        if (correct) {
            points++;
        }
    }

    public boolean proceedToNextQuestion() {
        currentQuestionIndex++;
        return currentQuestionIndex < questions.size();
    }

    public QuestionLevel getQuestionLevel() {
        return gameOptions.getQuestionLevel();
    }

    public String getCategoryName() {
        Optional<String> category = qds.getQuizCategories().stream()
                .filter(c -> c.getId() == gameOptions.getCategoryId())
                .map(CategoriesDto.CategoryDto::getName)
                .findAny();
        return category.orElse(null);
    }

    public int getPoints() {
        return points;
    }
}
