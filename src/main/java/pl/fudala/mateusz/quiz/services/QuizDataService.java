package pl.fudala.mateusz.quiz.services;

import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.fudala.mateusz.quiz.dto.CategoriesDto;
import pl.fudala.mateusz.quiz.dto.CategoryQuestionCountInfoDto;
import pl.fudala.mateusz.quiz.dto.QuestionsDto;
import pl.fudala.mateusz.quiz.frontend.GameOptions;
import pl.fudala.mateusz.quiz.frontend.QuestionLevel;

import java.net.URI;
import java.util.List;

@Service
@Log
public class QuizDataService {

    public List<CategoriesDto.CategoryDto> getQuizCategories() {
        RestTemplate rt = new RestTemplate();
        CategoriesDto categories =
                rt.getForObject("https://opentdb.com/api_category.php", CategoriesDto.class);
        log.info("Quiz categories: " + categories.getCategories());
        return categories.getCategories();
    }

    public List<QuestionsDto.QuestionDto> getQuizQuestions(GameOptions go) {
        CategoryQuestionCountInfoDto cqc = getCategoryQuestionCount(go.getCategoryId());
        int availableQuestionCount = cqc.getQuestionCountForQuestionLevel(go.getQuestionLevel());
        if (availableQuestionCount >= go.getNumberOfQuestions()) {
            return getQuizQuestions(go.getNumberOfQuestions(), go.getCategoryId(), go.getQuestionLevel());
        } else {
            return getQuizQuestions(availableQuestionCount, go.getCategoryId(), go.getQuestionLevel());
        }


    }

    public List<QuestionsDto.QuestionDto> getQuizQuestions(
            int numberOfQuestions, int categoryId, QuestionLevel questionLevel) {
        RestTemplate rt = new RestTemplate();

        URI uri = UriComponentsBuilder.fromHttpUrl("https://opentdb.com/api.php")
                .queryParam("amount", numberOfQuestions)
                .queryParam("category", categoryId)
                .queryParam("difficulty", questionLevel.name().toLowerCase())
                .build().toUri();
        log.info("Quiz question retrieve URL: " + uri);

        QuestionsDto questions = rt.getForObject(uri, QuestionsDto.class);
        log.info("Quiz questions: Open Trivia DB response code" + questions.getResponseCode() +
                ". Content: " + questions.getResults());
        return questions.getResults();
    }


    private CategoryQuestionCountInfoDto getCategoryQuestionCount(int categoryId) {
        RestTemplate rt = new RestTemplate();

        URI uri = UriComponentsBuilder.fromHttpUrl("https://opentdb.com/api_count.php")
                .queryParam("category", categoryId)
                .build().toUri();
        log.info("Quiz category question count retrieve URL: " + uri);
        CategoryQuestionCountInfoDto result = rt.getForObject(uri, CategoryQuestionCountInfoDto.class);
        log.info("Quiz category question count content: " + result);
        return result;
    }

}
