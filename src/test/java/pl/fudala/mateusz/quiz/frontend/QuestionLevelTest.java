package pl.fudala.mateusz.quiz.frontend;

import org.junit.jupiter.api.Test;

import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.*;
import static pl.fudala.mateusz.quiz.frontend.QuestionLevel.*;

class QuestionLevelTest {

    @Test
    void shouldReturnNextAsMediumWhenQuestionLevelIsEasy() {
        EnumSet<QuestionLevel> given = EnumSet.of(EASY);
        QuestionLevel result = calculateNextLevel(given);
        assertEquals(MEDIUM, result);
    }

    @Test
    void shouldReturnNullWhenQuestionLevelIsNull(){
        EnumSet<QuestionLevel> given = null;
        QuestionLevel result = calculateNextLevel(given);
        assertNull(result);
    }

    @Test
    void shouldReturnNullWhenLevelIsNotInRange(){
        EnumSet<QuestionLevel> given = EnumSet.noneOf(QuestionLevel.class);
        QuestionLevel result = calculateNextLevel(given);
        assertNull(result);
    }

    @Test
    void shouldReturnEasyWhenQuestionLevelIsMediumAndHard() {
        EnumSet<QuestionLevel> given = EnumSet.of(MEDIUM, HARD);
        QuestionLevel result = calculateNextLevel(given);
        assertEquals(EASY, result);
    }
}